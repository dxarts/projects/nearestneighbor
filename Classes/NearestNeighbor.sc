NearestNeighbor {
	var <points, <resolution, <normalizations, <expNormalizations, useMatrix, calcResolution;
	var <matrix, <matrixPoints;
	var <dim;
	var <halfRes;
	var <translatedPoints;
	var shape, maxTuples, division, halfDiv;
	var oldRes, <timer;
	var <matrixError, <pointErrors;

	*new { |points, resolution, normalizations, expNormalizations = false, useMatrix = true, calcResolution = true|
		^super.newCopyArgs(points, resolution, normalizations, expNormalizations, useMatrix, calcResolution).init
	}

	init {

		timer = Main.elapsedTime;
		// in case of Point or Cartesian
		points = points.collect{ |point| point.asArray };

		// get the dimensions
		dim = points[0].size;

		// check all points for same dimensions
		points.do{ |point| (dim != point.size).if({ Error("Points do not all have the same number of dimensions").throw }) };

		this.checkNormalizations;

		// translate the points into the positive quadrant, so all values are 0 to 1
		translatedPoints = normalizations.notNil.if({
			this.checkForExpZero;
			points.collect{ |point|
				this.translateQuadrant(point, nil, expNormalizations)
			}
		}, {
			points
		});

		useMatrix.if({
			// if resolution is nil, guess what should cover the amount of points supplied
			resolution = calcResolution.if({ 2 }, {
				resolution ?? { (points.size**dim.reciprocal * 10).asInteger }
			});
			this.calculateMatrix;
			this.testMatrix(calcResolution)
		});

		timer = Main.elapsedTime - timer;

	}

	checkNormalizations {

		normalizations.isKindOf(Array).if({
			// if its an array with all dimensions, do nothing else check if its a single [min, max]
			(normalizations.size != dim).if({
				// if not a symbol, assume its a [min, max]
				((normalizations.size == 2) and: normalizations[0].isKindOf(SimpleNumber)).if({
					normalizations = normalizations.dup(points.flop.size)
				}, {
					// if its not one, nor all dimensions, throw error, don't know what to do
					Error("Normalizations and points do not have the same number of dimensions").throw
				})
			})
		}, {
			// if it's true, set to the max and min
			normalizations.notNil.if({
				normalizations.if({
					normalizations = points.flop.collect{ |axis|
						[axis.flat.minItem, axis.flat.maxItem] };
				})
			})
		})
	}

	checkForExpZero {
		expNormalizations.do{ |bool, i|
			(bool and: normalizations[i][0] == 0.0).if({
				normalizations[i][0] = 0.00000001
			})
		}
	}

	calculateMatrix {
		// the resulting shape of the matrix depending on resolution and dimensions
		shape = resolution.dup(dim) ++ dim;

		// the max number of tuples desired is the size of the matrix
		// (where each element in the matrix is a n-dimensional point)
		maxTuples = (resolution**dim);

		oldRes = resolution;

		// max size of matrix???
		while( { maxTuples > (2**22) }, {
			resolution = resolution - 1;
			maxTuples = (resolution**dim);
		});

		maxTuples = maxTuples.asInteger;

		(oldRes != resolution).if({
			("Resolution was" + oldRes ++ ". Reset to" + resolution).warn
		});

		// the step of each n-dimensional "box"
		division = resolution.reciprocal;

		// get the center of each box
		halfDiv = division * 0.5;

		// fill matrix with n-dimensional points that represent the center of each "box"
		matrix = dim.collect{ resolution.collect{ |i| halfDiv * (2*i+1) } };

		// get the permutations (actual points) of the centers
		matrix = matrix.allTuples(maxTuples);

		// reshape the matrix to the correct size
		matrixPoints = matrix.reshape(*shape);

		// calculate the index distance from the center of the box to the points
		matrix = matrixPoints.deepCollect(dim, { |val|
			translatedPoints.collect{ |point| this.dist(val, point, false) }.minIndex
		})
	}

	matrixIndex { |point, useNormalization = true|
		var nPoint;
		point = point.asArray;
		point = useNormalization.if({
			this.translateQuadrant(point, nil, expNormalizations) * resolution
		}, {
			point * resolution
		});
		nPoint = matrix;
		point.do{ |val| nPoint = nPoint[val.clip(0, resolution - 1)] };
		^nPoint
	}

	// returns normalized distance
	matrixDistance { |point, useNormalization = true|
		point = point.asArray;
		useNormalization.if({
			point =  this.translateQuadrant(point, nil, expNormalizations);
		});
		^this.dist(translatedPoints[this.matrixIndex(point, false)], point)
	}

	// uses normalized distance
	nearestN { |point, axes, useNormalization = false, useDTW = true|
		axes = axes ?? { (0..dim-1) };
		point = point.asArray;
		// if user has give full point data, reduce to the given axes
		(point.size != axes.size).if({ point = point[axes] });
		useNormalization.if({
			point = this.translateQuadrant(point, axes, expNormalizations);
		});
		^translatedPoints.collect{ |thisPoint|
			this.dist(thisPoint[axes], point, useDTW)
		}
	}

	// returns normalized distance
	nearestDistance { |point, axes, useNormalization = false, useDTW = true|
		^this.nearestN(point, axes, useNormalization, useDTW).minItem
	}

	nearestIndex { |point, axes, useNormalization = false, useDTW = true|
		^this.nearestN(point, axes, useNormalization, useDTW).minIndex
	}

	orderedIndices { |point, axes, useNormalization = false, useDTW = true|
		^this.nearestN(point, axes, useNormalization, useDTW).order
	}

	dist { |point1, point2, useDTW = true|
		var dist;
		point1 = point1.asArray;
		point2 = point2.asArray;
		(point1.size == point2.size).not.if({
			"The two points do not have the same number of dimensions".warn
		});

		point1.do{ |point, i|
			(point.isKindOf(Array) or: point2[i].isKindOf(Array)).if({
				useDTW.if({
					dist = dist.add(DTW.calc(point.asArray, point2[i].asArray))
				}, {
					dist = dist.add(point.asArray.sum/point.asArray.size - point2[i].asArray.sum/point2[i].asArray.size)
				})
			}, {
				dist = dist.add(point - point2[i])
			})
		};
		^dist.squared.sum.sqrt

	}

	translateQuadrant { |point, axes, isExp|
		var selector;
		axes = axes ?? { (0..dim-1) };
		isExp.notNil.if({
			isExp.isKindOf(Array).not.if({
				isExp = [isExp]
			})
		}, {
			isExp = [false]
		});
		^normalizations.notNil.if({
			point.asArray.collect{ |pointAxis, i|
				selector = isExp.wrapAt(i).if({ 'explin' }, { 'linlin' });
				pointAxis.perform(selector, normalizations[axes[i]][0], normalizations[axes[i]][1], 0.0, 1.0)
			}
		}, {
			"Warning, there are no normalizations set".warn;
			point
		})
	}

	testMatrix { |recalcMatrix = false|
		pointErrors = [];
		points.do{ |point, pointNum|
			(this.matrixIndex(point) != pointNum).if({
				pointErrors = pointErrors.add(pointNum)
			})
		};
		matrixError = pointErrors.size/points.size;
		matrixError.postln;
		(recalcMatrix and: (matrixError > 0.0)).if({
			resolution = (resolution * 1.5).round.asInteger;
			this.calculateMatrix;
			this.testMatrix(true)
		})
	}

}
