DTW {
	// https://en.wikipedia.org/wiki/Dynamic_time_warping
	*calc { |signal1, signal2, window|
		var dtwMatrix, size1, size2;
		size1 = signal1.size;
		size2 = signal2.size;
		dtwMatrix = (size1 + 1).collect{
			(size2 + 1).collect{
				inf
			}
		};

		window = window ?? { size2 };
		window = window.max(abs(size1-size2));

		(1..size1).do{ |i|
			(1.max(i-window)..size2.min(i+window)).do{ |j|
				dtwMatrix[i][j] = 0
			}
		};

		dtwMatrix[0][0] = 0;

		(1..size1).do{ |i|
			(1.max(i-window)..size2.min(i+window)).do{ |j|
				var cost;
				cost = (signal1[i-1] - signal2[j-1]).abs;
				dtwMatrix[i][j] = cost + [
					dtwMatrix[i-1][j],    // insertion
					dtwMatrix[i][j-1],    // deletion
					dtwMatrix[i-1][j-1]].minItem    // match
			}
		};

		^dtwMatrix[size1][size2]
	}

	*pseqSim { |pitchSequence, data, maxWindow|
		var minWindow, currentIndex = 0, windowedData, minDTW, masterData, order;
		masterData = [];
		// max and min window looking sizes
		maxWindow = maxWindow ?? { pitchSequence.size * 2 };
		minWindow = pitchSequence.size;

		// iterate through the data
		while({ currentIndex < (data.size - maxWindow) }, {
			// collect all the windows
			// get the data from the current index to the desired window size
			windowedData = (maxWindow - minWindow).collect{ |inc|
				data[currentIndex..currentIndex + minWindow + inc - 1]
			};
			// find which window is the closest match
			minDTW = windowedData.collect{ |thisWindow|
				DTW.calc(pitchSequence, thisWindow)
			}.minIndex;
			// move forward in the data by that window size
			currentIndex = currentIndex + minDTW + minWindow;

			// add that window and the current position to an array
			masterData = masterData.add([windowedData[minDTW], currentIndex]);
		});

		// order the saved windows by lowest DTW with the original sequence
		^masterData[masterData.flop[0].collect{ |thisSeq|
			DTW.calc(pitchSequence, thisSeq)
		}.order]

	}
}